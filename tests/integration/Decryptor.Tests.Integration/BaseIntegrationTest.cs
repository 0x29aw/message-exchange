﻿using Commons.Data;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using Xunit;

namespace Decryptor.Tests.Integration
{
    public class BaseIntegrationTest : IClassFixture<TestAppFactory>
    {
        protected HttpClient client;
        internal DataContext dataContext;

        public BaseIntegrationTest(TestAppFactory factory)
        {
            client = factory.CreateClient();
            dataContext = (DataContext)factory.Services.GetRequiredService<IDataContext>();
        }
    }
}
