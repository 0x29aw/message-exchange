﻿using Commons.Dto;
using Commons.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Decryptor.Tests.Integration.Controllers
{
    public class CyphersControllerTest : BaseIntegrationTest
    {
        private const string DECRYPT_ENDPOINT = "Decrypt";
        private const string ENCRYPTED = "zH05TaXKykSKPRyPRW1n+waCaP6AnhTmrLVPMYhCWM4=";
        private const string DECRYPTED = "integration_test_message";
        private readonly Guid key = new Guid("5ce375e5-4fd8-437f-a3d5-90a843deb86d");
        private readonly Cypher cypher = new();

        public CyphersControllerTest(TestAppFactory factory)
            : base(factory)
        {
            prepareData();
        }
                
        [Fact]
        public async Task PostAsync_ShouldReturnDecryptedMessageAsync()
        {
            var dtoJson = JsonConvert.SerializeObject(new CypherDto
            {
                Id = cypher.Id,
                Key = key,
            });
            var buffer = Encoding.UTF8.GetBytes(dtoJson);
            var postBody = new ByteArrayContent(buffer);
            postBody.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Json);

            var result = await client.PostAsync(DECRYPT_ENDPOINT, postBody);
            var stringResult = await result.Content.ReadAsStringAsync();

            result.StatusCode.Should().Be((HttpStatusCode)StatusCodes.Status418ImATeapot);
            stringResult.Should().Be(DECRYPTED);
        }

        private void prepareData()
        {
            cypher.Message = ENCRYPTED;
            dataContext.Cyphers.Add(cypher);
            dataContext.SaveChanges();
        }
    }
}
