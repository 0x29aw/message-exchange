﻿using Commons.Dto;
using Commons.Models;
using Commons.Queries.GetCypher;
using Commons.Services;
using Decryptor.Controllers;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading;
using Xunit;

namespace Decryptor.Tests.Controllers
{
    public class DecryptControllerTest
    {
        private readonly DecryptController controller;
        private readonly Mock<IMediator> mediatorMock = new();
        private readonly Mock<IEncryptionService> serviceMock = new();

        public DecryptControllerTest()
        {
            controller = new(mediatorMock.Object, serviceMock.Object);
        }

        [Fact]
        public async void PostAsync_ShouldReturnDecryptedMessageWith418Status()
        {
            var dto = new CypherDto()
            {
                Id = 4321,
                Key = Guid.NewGuid(),
            };
            serviceMock.Setup(x => x.Decrypt("test_encryted", dto.Key))
                .Returns("test_decrypted");
            mediatorMock
                .Setup(x =>x.Send(It.Is<GetCypherQuery>(q => q.Id == dto.Id), default(CancellationToken)))
                .ReturnsAsync(new Cypher
                    {
                        Id = 4321,
                        Message = "test_encryted",
                    }
                );

            var result = await controller.PostAsync(dto) as ObjectResult;

            result.Should().NotBeNull();
            result!.StatusCode.Should().Be(StatusCodes.Status418ImATeapot);
            result!.Value.Should().Be("test_decrypted");
        }

        [Fact]
        public async void PostAsync_WhenQueryReturnNull_ShouldReturn404NotFound()
        {
            var dto = new CypherDto() { Id = 4321 };
            mediatorMock
                .Setup(x =>x.Send(It.Is<GetCypherQuery>(q => q.Id == dto.Id), default(CancellationToken)))
                .ReturnsAsync(() => null!);

            var result = await controller.PostAsync(dto) as ObjectResult;

            result.Should().NotBeNull();
            result!.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            result!.Value.Should().Be("Cypher not found.");
        }
    }
}
