﻿using Decryptor.Middlewares;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit;

namespace Decryptor.Tests.Middlewares;

public class ExceptionFilterMiddlewareTest
{
    private readonly ExceptionLoggerMiddleware middleware;
    private readonly Mock<RequestDelegate> requestDelegateMock = new();
    private readonly Mock<ILogger<ExceptionLoggerMiddleware>> loggerMock = new();

    public ExceptionFilterMiddlewareTest()
    {
        middleware = new(requestDelegateMock.Object, loggerMock.Object);
    }

    [Fact]
    public async void InvokeAsync_WhenDelegateRunCorrectly_ShouldDoNothing()
    {
        var contextMock = new Mock<HttpContext>();

        await middleware.InvokeAsync(contextMock.Object);

        requestDelegateMock.Verify(x => x(contextMock.Object), Times.Once);
        requestDelegateMock.VerifyNoOtherCalls();
        loggerMock.VerifyNoOtherCalls();
    }

    [Fact]
    public async void InvokeAsync_WhenDelegateThrowException_ShouldLogAndSetInternalErrorCode()
    {
        var contextMock = new Mock<HttpContext>();
        var responseMock = new Mock<HttpResponse>();
        responseMock.SetupProperty(x => x.StatusCode);
        contextMock.Setup(x => x.Response)
            .Returns(responseMock.Object);
        var exception = new Exception();
        requestDelegateMock.Setup(x => x(contextMock.Object))
            .Throws(exception);

        await middleware.InvokeAsync(contextMock.Object);

        contextMock.Object.Response.StatusCode.Should().Be(StatusCodes.Status500InternalServerError);
        loggerMock.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                exception,
                (Func<It.IsAnyType, Exception?, string>)It.IsAny<object>()
            ),
            Times.Once
        );
        requestDelegateMock.Verify(x => x(contextMock.Object), Times.Once);
        requestDelegateMock.VerifyNoOtherCalls();
        loggerMock.VerifyNoOtherCalls();
    }
}
