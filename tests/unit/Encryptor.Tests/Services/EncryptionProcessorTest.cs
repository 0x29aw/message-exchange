using Commons.Commands.CreateCypher;
using Commons.Dto;
using Commons.Services;
using Encryptor.Services;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Moq;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using Xunit;

namespace Encryptor.Tests.Services;

public class EncryptionProcessorTest
{
    private readonly EncryptionProcessor encryptionProcessor;
    private readonly Mock<IEncryptionService> encryptionServiceMock = new();
    private readonly Mock<IMediator> mediatorMock = new();
    private readonly Mock<HttpClient> httpClientMock = new();
    private readonly Guid testKey = Guid.NewGuid();
    private const string TEST_PLAIN_MSG = "test_plain";
    private const string TEST_ENCRYPTED = "test_encrypted";
    private int? addedCypherId = 321;

    public EncryptionProcessorTest()
    {
        encryptionProcessor = new(encryptionServiceMock.Object, mediatorMock.Object, httpClientMock.Object);
    }

    [Fact]
    public async void GetEncryption_ShouldReturnCorrectEncryptionData()
    {
        configureAddingMocks(addedCypherId);
        configureRequestMocks(StatusCodes.Status418ImATeapot, TEST_PLAIN_MSG);

        var result = await encryptionProcessor.GetEncryption(TEST_PLAIN_MSG, testKey);

        result.Should().Contain(TEST_ENCRYPTED);
        result.Should().Contain(TEST_PLAIN_MSG);
        result.Should().Contain(testKey.ToString());
    }

    [Fact]
    public async void GetEncryption_WhenAddingCypherFailed_ShouldReturnErrorInfo()
    {
        configureAddingMocks(null);
        configureRequestMocks(StatusCodes.Status418ImATeapot, TEST_ENCRYPTED);

        var result = await encryptionProcessor.GetEncryption(TEST_PLAIN_MSG, testKey);

        result.Should().Contain("Failed add new Cypher.");
    }

    [Fact]
    public async void GetEncryption_WhenDecryptionRequestFailed_ShouldReturnErrorInfo()
    {
        configureAddingMocks(addedCypherId);
        configureRequestMocks(999, TEST_ENCRYPTED);

        var result = await encryptionProcessor.GetEncryption(TEST_PLAIN_MSG, testKey);

        result.Should().Contain("Api response 999.");
    }

    private void configureAddingMocks(int? returnedCypherId)
    {
        encryptionServiceMock
            .Setup(x => x.Encrypt(TEST_PLAIN_MSG, testKey))
            .Returns(TEST_ENCRYPTED);
        mediatorMock
            .Setup(x => x.Send(
                It.Is<CreateCypherCommand>(m => m.Cypher.Message == TEST_ENCRYPTED),
                default(CancellationToken)
            ))
            .ReturnsAsync(returnedCypherId);
    }

    private void configureRequestMocks(int responseCode, string responseMsg)
    {
        var checkIfContextContainsDto = (ByteArrayContent content) =>
        {
            var dto = JsonConvert
                .DeserializeObject<CypherDto>(Encoding.UTF8.GetString(content.ReadAsByteArrayAsync().Result));
            return dto.Id == addedCypherId && dto.Key == testKey;
        };

        var testResponse = new HttpResponseMessage((HttpStatusCode)responseCode)
        {
            Content = new StringContent(responseMsg),
        };

        httpClientMock.Setup(x => 
            x.SendAsync(
                It.Is((HttpRequestMessage request) => checkIfContextContainsDto((ByteArrayContent)request.Content!)),
                It.IsAny<CancellationToken>())
            )
            .ReturnsAsync(testResponse);

    }
}
