﻿namespace Commons.Tests.Services;

public class EncryptionAesServiceTest
{
    private readonly EncryptionAesService service = new();

    [Theory]
    [MemberData(nameof(TheoryData))]
    public void Encrypt_ShouldEncryptCorrectly(string plainMessage, Guid key, string encrypted)
    {
        var res = service.Encrypt(plainMessage, key);

        res.Should().Be(encrypted);
    }

    [Theory]
    [MemberData(nameof(TheoryData))]
    public void Decrypt_ShouldDecryptCorrectly(string plainMessage, Guid key, string encrypted)
    {
        var res = service.Decrypt(encrypted, key);

        res.Should().Be(plainMessage);
    }

    public static object[] TheoryData = new object[]
    {
        new object[] { "", "00000000-0000-0000-0000-000000000000", "BGhY27hvmIrBy0Dprd2vRg==" },
        new object[] { "", "e26dd9d1-3811-44d5-91a6-4252ee43a818", "CniGMWrGAvR+JVLqUjgSbA==" },
        new object[] { "test", "00000000-0000-0000-0000-000000000000", "YQwIlSqqOJ/5O8TZXn0KJw==" },
        new object[] { "test", "45c6871a-d552-4162-a835-9eeeaea4b26f", "HJFKuG+rOkwrtZCb9OgQ5g==" },
        new object[] { "test very large message", "f72296e1-83b4-462b-b836-76d82bb6b8e7", "0ZLq8TXqyccdanksi1ED6u9PwGvndyyS3PaN5NyGRTo=" },
        new object[] { "000000000000000000000000", "fdfbd86d-3e4b-40e9-8f99-efe35ab13cbc", "J6VV7BxzY2VVtEJ2lMu/PRz6nZfah6FLUaWXlzxqfUw=" },
        new object[] { "                        ", "81a090ad-f44f-4c7c-ab72-055a989453e1", "qtALXK2mZiywJ5k1Un/00ficVONa1sE06THUf9IqpYM=" },
        new object[] { "........................", "81a090ad-f44f-4c7c-ab72-055a989453e1", "+06fihMNJAip81Nkvsn9OQUBSd8coShcb1XEcMRRFM8=" },
    };
}
