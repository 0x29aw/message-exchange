﻿using MediatR;

namespace Commons.Tests;

public class ServiceCollectionExtensionsTest
{
    [Fact]
    public void ConfigureServices_ShouldAddAllRequiredServices()
    {
        var services = new ServiceCollection();
        var configuration = new Mock<IConfiguration>(MockBehavior.Strict);
        configuration.Setup(x => x["ConnectionStrings.DbConnection"])
            .Returns("test_connection_string");

        services.ConfigureServices(configuration.Object);

        services.Should().HaveCount(18);
        services.Should().Contain(x => x.ServiceType == typeof(IDataContext) && x.ImplementationType == typeof(DataContext));
        services.Should().Contain(x => x.ServiceType == typeof(IDAOContext) && x.ImplementationType == typeof(DAOContext) && x.Lifetime == ServiceLifetime.Transient);
        services.Should().Contain(x => x.ServiceType == typeof(ICreateRepository<Cypher>) && x.ImplementationType == typeof(CypherCreateRepository) && x.Lifetime == ServiceLifetime.Scoped);
        services.Should().Contain(x => x.ServiceType == typeof(IReadRepository<Cypher>) && x.ImplementationType == typeof(CypherReadRepository) && x.Lifetime == ServiceLifetime.Scoped);
        services.Should().Contain(x => x.ServiceType == typeof(IEncryptionService) && x.ImplementationType == typeof(EncryptionAesService) && x.Lifetime == ServiceLifetime.Transient);
        services.Should().Contain(x => x.ServiceType == typeof(IMediator));
        services.Should().Contain(x => x.ServiceType == typeof(IRequestHandler<CreateCypherCommand, long?>) && x.ImplementationType == typeof(CreateCypherCommandHandler));
        services.Should().Contain(x => x.ServiceType == typeof(IRequestHandler<GetCypherQuery, Cypher?>) && x.ImplementationType == typeof(GetCypherQueryHandler));
    }

    [Fact]
    public void MigrateDatabase_ShouldCallIDataContextServiceMigrate()
    {
        var fakeContext = new Mock<IDataContext>();
        var services = new ServiceCollection();
        services.AddTransient(x => fakeContext.Object);

        services.MigrateDatabase();

        fakeContext.Verify(x => x.Migrate(), Times.Once);
        fakeContext.VerifyNoOtherCalls();
    }
}
