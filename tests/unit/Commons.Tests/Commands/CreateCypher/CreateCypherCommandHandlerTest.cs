﻿namespace Commons.Tests.Commands.CreateCypher;

public class CreateCypherCommandHandlerTest
{
    private readonly CreateCypherCommandHandler handler;
    private readonly Mock<ICreateRepository<Cypher>> repositoryMock = new(MockBehavior.Strict);
    private readonly Mock<ILogger<CreateCypherCommandHandler>> loggerMock = new();

    public CreateCypherCommandHandlerTest()
    {
        handler = new(repositoryMock.Object, loggerMock.Object);
    }

    [Fact]
    public async void Handle_WhenRepositoryAddSuccesfully_ShouldReturnAddedId()
    {
        var cypher = new Cypher { Message = "test_message" };
        var command = new CreateCypherCommand(cypher);
        repositoryMock.Setup(x => x.Create(cypher))
            .Returns(321);

        var result = await handler.Handle(command, CancellationToken.None);

        result.Should().Be(321);
        loggerMock.VerifyNoOtherCalls();
    }

    [Fact]
    public async void Handle_WhenRepositoryThrowExeption_ShouldLogErrorAndReturnNull()
    {
        var cypher = new Cypher { Message = "test_message" };
        var command = new CreateCypherCommand(cypher);
        var exception = new Exception();
        repositoryMock.Setup(x => x.Create(cypher))
            .Throws(exception);

        var result = await handler.Handle(command, CancellationToken.None);

        result.Should().Be(null);
        loggerMock.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                exception,
                (Func<It.IsAnyType, Exception?, string>)It.IsAny<object>()
            ),
            Times.Once
        );
        loggerMock.VerifyNoOtherCalls();
    }
}
