﻿using Commons.Queries.GetCypher;

namespace Commons.Tests.Queries.GetCypher;

public class GetCypherQueryHandlerTest
{
    private readonly GetCypherQueryHandler handler;
    private readonly Mock<IReadRepository<Cypher>> repositoryMock = new(MockBehavior.Strict);
    private readonly Mock<ILogger<GetCypherQueryHandler>> loggerMock = new();

    public GetCypherQueryHandlerTest()
    {
        handler = new(repositoryMock.Object, loggerMock.Object);
    }

    [Fact]
    public async Task Handle_WhenRepositoryReturnCypherEntity_ShouldReturnThisEntityAsync()
    {
        var entity = new Cypher();
        repositoryMock.Setup(x => x.GetById(123))
            .Returns(entity);

        var result = await handler.Handle(new GetCypherQuery(123), CancellationToken.None);

        result.Should().Be(entity);
        loggerMock.VerifyNoOtherCalls();
    }



    [Fact]
    public async void Handle_WhenRepositoryThrowExeption_ShouldLogErrorAndReturnNull()
    {
        var exception = new Exception();
        repositoryMock.Setup(x => x.GetById(321))
            .Throws(exception);

        var result = await handler.Handle(new GetCypherQuery(321), CancellationToken.None);

        result.Should().Be(null);
        loggerMock.Verify(x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                exception,
                (Func<It.IsAnyType, Exception?, string>)It.IsAny<object>()
            ),
            Times.Once
        );
        loggerMock.VerifyNoOtherCalls();
    }
}
