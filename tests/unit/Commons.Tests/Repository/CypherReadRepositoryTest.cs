﻿namespace Commons.Tests.Repository;

public class CypherReadRepositoryTest
{
    private readonly CypherReadRepository repository;
    private readonly Mock<IDataContext> contextMock = new(MockBehavior.Strict);

    public CypherReadRepositoryTest()
    {
        repository = new(contextMock.Object);
    }

    [Fact]
    public void GetById_WhenEntityExists_ShouldReturnFoundEntity()
    {
        var cypher = new Cypher();
        contextMock.Setup(x => x.Cyphers.Find(123L))
            .Returns(cypher);

        var result = repository.GetById(123L);

        result.Should().Be(cypher);
    }

    [Fact]
    public void GetById_WhenEntityDoesntExists_ShouldReturnNull()
    {
        contextMock.Setup(x => x.Cyphers.Find(123L))
            .Returns<Cypher?>(null);

        var result = repository.GetById(123L);

        result.Should().BeNull();
    }
}
