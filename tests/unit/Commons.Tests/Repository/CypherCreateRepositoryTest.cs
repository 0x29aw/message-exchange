﻿namespace Commons.Tests.Repository;

public class CypherCreateRepositoryTest
{
    private readonly CypherCreateRepository repository;
    private readonly Mock<IDAOContext> contextMock = new(MockBehavior.Strict);

    public CypherCreateRepositoryTest()
    {
        repository = new(contextMock.Object);
    }

    [Fact]
    public void Create_ShouldReturnContextQueryResult()
    {
        var newCypher = new Cypher
        {
            Message = "test_message",
        };
        dynamic queryRes = new ExpandoObject();
        queryRes.Id = 321;
        contextMock.Setup(x => x.QuerySingle(It.IsAny<string>(), newCypher))
            .Returns(queryRes);

        var createResult = repository.Create(newCypher);

        createResult.Should().Be(321);
    }

}
