﻿using Commons.Dto;
using Commons.Queries.GetCypher;
using Commons.Services;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Decryptor.Controllers;

[ApiController]
[Route("[controller]")]
public class DecryptController : ControllerBase
{
    private readonly IMediator mediator;
    private readonly IEncryptionService encryptionService;

    public DecryptController(IMediator mediator, IEncryptionService encryptionService)
    {
        this.mediator = mediator;
        this.encryptionService = encryptionService;
    }

    [HttpPost]
    [ProducesResponseType(StatusCodes.Status418ImATeapot)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> PostAsync(CypherDto cypherDto)
    {
        var entity = await mediator.Send(new GetCypherQuery(cypherDto.Id));
        if(entity == null)
        {
            return NotFound("Cypher not found.");
        };

        var plainMessage = encryptionService.Decrypt(entity.Message, cypherDto.Key);
        return StatusCode(StatusCodes.Status418ImATeapot, plainMessage);
    }
}
