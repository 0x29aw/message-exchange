﻿namespace Decryptor.Middlewares;

public class ExceptionLoggerMiddleware
{
    private readonly RequestDelegate next;
    private readonly ILogger logger;

    public ExceptionLoggerMiddleware(RequestDelegate next, ILogger<ExceptionLoggerMiddleware> logger)
    {
        this.logger = logger;
        this.next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await next(context);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Failed action.");

            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
        }
    }
}
