﻿using System.Security.Cryptography;

namespace Commons.Services;

internal class EncryptionAesService : IEncryptionService
{
    private readonly SymmetricAlgorithm algorithm = Aes.Create();

    public EncryptionAesService()
    {
        algorithm.IV = new byte[16];
    }

    public string Encrypt(string plainMessage, Guid key)
    {
        algorithm.Key = Convert.FromBase64String(key.ToString("n"));

        using (MemoryStream memoryStream = new())            
        using (CryptoStream cryptoStream = new(memoryStream, algorithm.CreateEncryptor(), CryptoStreamMode.Write))
        {
            using (StreamWriter streamWriter = new(cryptoStream))
            {
                streamWriter.Write(plainMessage);
            }
            return Convert.ToBase64String(memoryStream.ToArray());
        }
    }

    public string Decrypt(string encryptedMessage, Guid key)
    {
        algorithm.Key = Convert.FromBase64String(key.ToString("n"));

        using (MemoryStream memoryStream = new(Convert.FromBase64String(encryptedMessage)))
        using (CryptoStream cryptoStream = new(memoryStream, algorithm.CreateDecryptor(), CryptoStreamMode.Read))
        using (StreamReader streamReader = new(cryptoStream))
        {
            var result = streamReader.ReadToEnd();
            return result;
        }
    }
}
