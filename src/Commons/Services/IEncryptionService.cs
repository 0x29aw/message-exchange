﻿using Commons.Dto;

namespace Commons.Services;

/// <summary>
/// Service for encrypt and decrypt message with specific key
/// </summary>
public interface IEncryptionService
{
    /// <summary>
    /// Encrypt plain text message with specific key
    /// </summary>
    /// <param name="plainMessage">Message to encrypt</param>
    /// <param name="key">Key used to encrypt message</param>
    /// <returns>Encrypted message</returns>
    public string Encrypt(string plainMessage, Guid key);

    /// <summary>
    /// Decrypt cypher (encrypted message) with specific key
    /// </summary>
    /// <param name="encryptedMessage">Message to decrypt</param>
    /// <param name="key">Key used to decrypt message</param>
    /// <returns>Decrypted message</returns>
    public string Decrypt(string encryptedMessage, Guid key);
}
