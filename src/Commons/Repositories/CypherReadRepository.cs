﻿using Commons.Data;
using Commons.Models;

namespace Commons.Repositories;

internal class CypherReadRepository : IReadRepository<Cypher>
{
    private readonly IDataContext dataContext;

    public CypherReadRepository(IDataContext dataContext)
    {
        this.dataContext = dataContext;
    }

    public Cypher? GetById(long id)
    {
        return dataContext
            .Cyphers
            .Find(id);
    }
}
