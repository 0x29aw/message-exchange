﻿using Commons.Data;
using Commons.Models;

namespace Commons.Repositories;

internal class CypherCreateRepository : ICreateRepository<Cypher>
{
    private readonly IDAOContext context;

    public CypherCreateRepository(IDAOContext context)
    {
        this.context = context;
    }

    public long? Create(Cypher entity)
    {
        var sql = @$"INSERT INTO {nameof(DataContext.Cyphers)}
            ({nameof(Cypher.Message)}) 
            OUTPUT INSERTED.{nameof(Cypher.Id)}
            VALUES (@{nameof(Cypher.Message)});";

        return context.QuerySingle(sql, entity).Id;
    }
}
