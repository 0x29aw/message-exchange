﻿using Commons.Models;

namespace Commons.Repositories;

/// <summary>
/// Repository for adding record of TEntity type to storage
/// </summary>
/// <typeparam name="TEntity">Entity type</typeparam>
public interface ICreateRepository<TEntity>
    where TEntity : Entity
{
    /// <summary>
    /// Add specific entity to storage
    /// </summary>
    /// <param name="entity">Entity to add</param>
    /// <returns>Id of added record or null if failed</returns>
    public long? Create(TEntity entity);
}
