﻿using Commons.Models;

namespace Commons.Repositories;

/// <summary>
/// Repository for retrieving data of TEntity type
/// </summary>
/// <typeparam name="TEntity">Entity type</typeparam>
public interface IReadRepository<TEntity>
    where TEntity : Entity
{
    /// <summary>
    /// Retrieve one record of TEntity type by specific Id
    /// </summary>
    /// <param name="id">Id of record to get</param>
    /// <returns>Entity with specific Id, or null if not found or failed</returns>
    public TEntity? GetById(long id);
}
