﻿using Commons.Data;
using Commons.Models;
using Commons.Repositories;
using Commons.Services;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Commons;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<IDataContext, DataContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("DbConnection"))
        );
        services.AddTransient<IDAOContext, DAOContext>();
        services.AddScoped<ICreateRepository<Cypher>, CypherCreateRepository>();
        services.AddScoped<IReadRepository<Cypher>, CypherReadRepository>();
        services.AddTransient<IEncryptionService, EncryptionAesService>();
        services.AddMediatR(Assembly.GetExecutingAssembly());

        return services;
    }

    public static void MigrateDatabase(this IServiceCollection services)
    {
        var context = services.BuildServiceProvider().GetService<IDataContext>();
        if (context != null)
        {
            context.Migrate();
        }
    }
}
