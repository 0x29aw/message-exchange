﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Commons.Data;

internal class DAOContext : IDAOContext
{
    private readonly IConfiguration configuration;

    public DAOContext(IConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public dynamic QuerySingle(string sql, object? param = null)
    {
        using (var connection = new SqlConnection(configuration.GetConnectionString("DbConnection")))
        {
            return connection.QuerySingle(sql, param);
        }                
    }
}
