﻿namespace Commons.Data;

internal interface IDAOContext
{
    public dynamic QuerySingle(string sql, object? param = null);
}
