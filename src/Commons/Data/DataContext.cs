﻿using Commons.Models;
using Microsoft.EntityFrameworkCore;

namespace Commons.Data;

internal class DataContext : DbContext, IDataContext
{
    public DataContext(DbContextOptions options)
        : base(options)
    {
    }

    public DbSet<Cypher> Cyphers => Set<Cypher>();

    public void Migrate()
    {
        Database.Migrate();
    }
}
