﻿using Commons.Models;
using Microsoft.EntityFrameworkCore;

namespace Commons.Data;

internal interface IDataContext
{
    public DbSet<Cypher> Cyphers { get; }
    public void Migrate();
}
