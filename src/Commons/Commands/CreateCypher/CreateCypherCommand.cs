﻿using Commons.Models;
using MediatR;

namespace Commons.Commands.CreateCypher;

/// <summary>
/// Command for add new Cypher, that response added entity Id or null if failed
/// </summary>
public class CreateCypherCommand : IRequest<long?>
{
    public CreateCypherCommand(Cypher cypher)
    {
        Cypher = cypher;
    }

    /// <summary>
    /// New Cypher insance to add
    /// </summary>
    public Cypher Cypher { get; } = default!;
}
