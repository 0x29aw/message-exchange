﻿using Commons.Models;
using Commons.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Commons.Commands.CreateCypher;

internal class CreateCypherCommandHandler : IRequestHandler<CreateCypherCommand, long?>
{
    private ICreateRepository<Cypher> repository;
    private ILogger logger;

    public CreateCypherCommandHandler(
        ICreateRepository<Cypher> repository,
        ILogger<CreateCypherCommandHandler> logger
    )
    {
        this.repository = repository;
        this.logger = logger;
    }

    public Task<long?> Handle(CreateCypherCommand request, CancellationToken cancellationToken)
    {
        try
        {
            return Task.FromResult(repository.Create(request.Cypher));
        }
        catch(Exception e)
        {
            logger.LogError(e, "Failed create Cypher.");
            return Task.FromResult<long?>(null);
        }
    }
}
