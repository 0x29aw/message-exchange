﻿using Commons.Models;
using MediatR;

namespace Commons.Queries.GetCypher;

/// <summary>
/// Query for get Cypher instance, that response entity with specific Id or null if failed
/// </summary>
public class GetCypherQuery : IRequest<Cypher?>
{
    public GetCypherQuery(long id)
    {
        Id = id;
    }

    /// <summary>
    /// Id of Cypher to retrieve
    /// </summary>
    public long Id { get; }
}
