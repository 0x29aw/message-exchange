﻿using Commons.Models;
using Commons.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Commons.Queries.GetCypher;

internal class GetCypherQueryHandler : IRequestHandler<GetCypherQuery, Cypher?>
{
    private readonly IReadRepository<Cypher> readRepository;
    private readonly ILogger logger;

    public GetCypherQueryHandler(
        IReadRepository<Cypher> readRepository,
        ILogger<GetCypherQueryHandler> logger
    )
    {
        this.readRepository = readRepository;
        this.logger = logger;
    }

    public Task<Cypher?> Handle(GetCypherQuery request, CancellationToken cancellationToken)
    {
        try
        {
            return Task.FromResult(readRepository.GetById(request.Id));
        }
        catch(Exception e)
        {
            logger.LogError(e, $"Failed get Cypher by Id = {request.Id}.");
            return Task.FromResult<Cypher?>(null);
        }
    }
}
