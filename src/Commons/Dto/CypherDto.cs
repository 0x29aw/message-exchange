﻿namespace Commons.Dto;

public class CypherDto
{
    /// <summary>
    /// Id of encrypted message in DB
    /// </summary>
    public long Id { get; set; }
    /// <summary>
    /// Key to decrypt message
    /// </summary>
    public Guid Key { get; set; } = default!;
}
