﻿using Microsoft.Extensions.DependencyInjection;
using Encryptor.Utils;

namespace Encryptor;

public class Program
{
    private const int CYCLE_INTERVAL = 15_000;

    public static void Main()
    {
        var services = new ServiceCollection();
        services.ConfigureRequiredServices();
        var processor = EncryptionProcessorBuilder.BuildProcessor(services);

        Console.WriteLine("\nPress any key to stop.\n");

        TimerCallback callback = new(async (object? _) =>
            {
                var text = await processor.GetEncryption($"Message-{DateTime.Now}", Guid.NewGuid());
                Console.WriteLine(text);
                Console.WriteLine();
            }
        );
        var timer = new Timer(callback, null, 0, CYCLE_INTERVAL);

        Console.ReadKey();
    }
}


