﻿using Commons.Commands.CreateCypher;
using Commons.Dto;
using Commons.Models;
using Commons.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Mime;

namespace Encryptor.Services;

internal class EncryptionProcessor
{
    private const string ENDPOINT = "Decrypt";
    private readonly IEncryptionService encryptionService;
    private readonly IMediator mediator;
    private readonly HttpClient httpClient;

    public EncryptionProcessor(IEncryptionService encryptionService, IMediator mediator, HttpClient httpClient)
    {
        this.encryptionService = encryptionService;
        this.mediator = mediator;
        this.httpClient = httpClient;
    }

    /// <summary>
    /// Return formatted encryption data for message and encryption key
    /// </summary>
    /// <param name="message">Message to encrypt (plain text)</param>
    /// <param name="key">Encryption key</param>
    /// <returns>Formatted encryption data or error info if failed</returns>
    public async Task<string> GetEncryption(string message, Guid key)
    {
        try
        {
            var cypher = await addCypher(message, key);
            var dto = new CypherDto
            {
                Id = cypher.Id,
                Key = key,
            };
            var result = await sendDecryptionRequest(dto);

            return string.Format("{0,10} : {1}\n{2,10} : {3}\n{4,10} : {5}",
                "Encrypted", cypher.Message,
                "key", key,
                "decrypted", result
            );
        }
        catch (Exception e)
        {
            return $"Error: {e.Message}";
        }
    }

    private async Task<Cypher> addCypher(string message, Guid key)
    {
        var encryptionResult = encryptionService.Encrypt(message, key);
        var cypher = new Cypher()
        {
            Message = encryptionResult,
        };
        var addedCypherId = await mediator.Send(new CreateCypherCommand(cypher));
        if(! (addedCypherId > 0))
        {
            throw new Exception("Failed add new Cypher.");
        }
        cypher.Id = addedCypherId.Value;

        return cypher;
    }

    private async Task<string> sendDecryptionRequest(CypherDto dto)
    {
        var dtoJson = JsonConvert.SerializeObject(dto);
        var buffer = System.Text.Encoding.UTF8.GetBytes(dtoJson);
        var postBody = new ByteArrayContent(buffer);
        postBody.Headers.ContentType = new MediaTypeHeaderValue(MediaTypeNames.Application.Json);
        var response = await httpClient.PostAsync(ENDPOINT, postBody);
        if((int)response.StatusCode == StatusCodes.Status418ImATeapot)
        {
            return await response.Content.ReadAsStringAsync();
        }

        throw new Exception($"Api response {response.StatusCode}.");
    }
}
