﻿using Commons;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Encryptor;

internal static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureRequiredServices(this IServiceCollection services)
    {
        var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile($"appsettings.json")
            .AddJsonFile($"appsettings.Development.json", true)
            .Build();
        services.AddScoped<IConfiguration>(x => configuration);
        services.ConfigureServices(configuration);
        services.AddLogging(config =>
            config.AddConsole()
        );

        return services;
    }
}
