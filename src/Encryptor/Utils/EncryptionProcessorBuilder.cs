﻿using Commons.Services;
using Encryptor.Services;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Encryptor.Utils;

internal static class EncryptionProcessorBuilder
{
    public static EncryptionProcessor BuildProcessor(IServiceCollection services)
    {
        var encryptor = services.BuildServiceProvider().GetRequiredService<IEncryptionService>();
        var mediator = services.BuildServiceProvider().GetRequiredService<IMediator>();
        var configuration = services.BuildServiceProvider().GetRequiredService<IConfiguration>();

        string endpoint = configuration["EndpointUrl"];
        var httpClient = new HttpClient()
        {
            BaseAddress = new Uri(endpoint),
        };
        return new EncryptionProcessor(encryptor, mediator, httpClient);
    }
}
